#!/bin/bash
#
# Copyright (c) 2017, Max Maton <info@maxmaton.nl>
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

set -euo pipefail

version="1.0.0"


function bin2storage {
	base32 | tr '=' '*' | tr -d '\n'
}

function storage2bin {
	tr '*' '=' | base32 -d
}

function calculate_identifier {
	file=${1:?need a file to create an identifier for}
	sha1sum "$file" | cut -d' ' -f1 | xxd -r -p | bin2storage
}

function predict_blocks {
	datasize=${1:?need size of data in bytes}
	parsize=${2:?need size of recovery in bytes}

	datablocks=$((datasize / blocksize))
	parblocks=$((parsize / blocksize))
	
	seq -w 0 $datablocks | sed -e 's/^/D./g'
	seq -w 0 $parblocks | sed -e 's/^/P./g'
}

function missing_blocks {
	datasize=${1:?need size of data in bytes}
	parsize=${2:?need size of recovery in bytes}
	foundfile=${3:?need file with found blocks}

	comm -13 <(sort "$foundfile" | uniq) <(predict_blocks "$datasize" "$parsize") | rev | sort | rev
}

function encapsulate_block {
	block=${1:?need a block to encapsulate}
	identifier=${2:?need a file identifier}
	datasize=${3:?need size of data in bytes}
	parsize=${4:?need size of recovery in bytes}
	name=$(basename "$block" | tr '[:lower:].' '[:upper:] ')

	echo -n 'OCR1>'
	echo -n "${identifier}>"
	echo -n "${datasize}>"
	echo -n "${parsize}>"
	echo -n "${name}>"
	bin2storage < "$block"
	echo -n ">"
}

function to_graphics {
	dmtxwrite --margin 40 --encoding=x --resolution=300
}

function file2packets {
	file=${1:?need file to update}

	filetype=$(file --brief --mime-type "$file" | cut -d'/' -f1)
	if [[ "$filetype" = "text" ]]; then
		tr -dC '[:alnum:]*> \n\r' < "$file"
	else
		decode_graphics < "$file"
	fi
}

function decode_graphics {
	# shellcheck disable=SC2086
	dmtxread -n ${DMTX_READ_ARGS}
}

function set_block {
	file=${1:?need file to update}
	pos=${2:?need offset}
	data=${3:?need base32 data to set}

	storage2bin <<< "$data" | dd bs="$blocksize" count=1 seek="$((10#${pos}))" conv=notrunc of="$file" status=none
}

function unpack_block {
	dest=${1:?need a destination folder}
	block=${2:?need a block to unpack}

	header=$(cut -d'>' -f1 <<< "$block")
	identifier=$(cut -d'>' -f2 <<< "$block" | tr -d '/.')
	datasize=$(cut -d'>' -f3 <<< "$block")
	parsize=$(cut -d'>' -f4 <<< "$block")
	filename=$(cut -d'>' -f5 <<< "$block" | tr ' ' '.' | tr -d '/')
	data=$(cut -d'>' -f6 <<< "$block")

	if [[ "$header" != 'OCR1' ]]; then
		echo 'Found unknown block, skipping' 1>&2
		exit
	fi

	filetype=$(cut -d'.' -f1 <<< "$filename")
	position=$(cut -d'.' -f2 <<< "$filename")

	destfolder="${dest}/${identifier}"
	mkdir -p "$destfolder"

	if [ ! -f "${destfolder}/data" ]; then
		truncate -s "$datasize" "${destfolder}/data"
		truncate -s "$parsize" "${destfolder}/data.par2"
	fi

	outfile=/dev/null
	[[ "$filetype" = "D" ]] && outfile="${destfolder}/data"
	[[ "$filetype" = "P" ]] && outfile="${destfolder}/data.par2"

	set_block "$outfile" "$position" "$data" || echo "Warning: bad block: '$block'" 1>&2
	echo "$filename" >> "${destfolder}/found"

	totalblocks=$((datasize/blocksize + parsize/blocksize))
	[[ "$verbose" = "1" ]] && echo "$(tr '[:upper:]' '[:lower:]' <<< "$identifier"): $(sort < "${destfolder}/found" | uniq | wc -l)/${totalblocks}" || true
}

function make_redundant {
	file=${1:?need a file to create a parchive for}
	redundancy=${2:?need a redundancy percentage target}

	par2 create -s"${blocksize}" -n1 -r"${redundancy}" "$file"
}

function encode {
	for file in "$@"; do
		identifier=$(calculate_identifier "$file")
		tmpdir=$(mktemp -d)
		# shellcheck disable=SC2064
		trap "{ rm -rf '$tmpdir'; }" EXIT

		absolute=$(readlink -e "$file")
		filename=$(basename "$file")
		{
			echo "<html><head><title>${filename}</title><style>"
			echo "h1{ display:inline; }"
			echo "p{ display:inline; }"
			echo "div{ clear:right; }"
			echo "body{ font-size:9pt; color:#999; }"
			echo "@page{ margin:0;color:#999 }"
			echo "figure{ float:left; width: 19%; margin: 0; text-align: center; page-break-inside: avoid; }"
			echo "</style></head><body>"
			echo "<div><h1>${filename}</h1>"
			echo "<p>Stored using ocrbackup. See http://gitlab.com/thexa4/ocrbackup to restore.</div>"
		} > "${absolute}.ocr.html"
		cp "$file" "${tmpdir}/data"
		(
			cd "$tmpdir"
			make_redundant data "$redundancy"
			split -b"$blocksize" -d data d.
			split -b"$blocksize" -d data.vol*.par2 p.

			datasize=$(stat --format="%s" data)
			parsize=$(stat --format="%s" data.vol*.par2)

			counter=0
			find . -name 'd.*' -or -name 'p.*' | cut -d'/' -f2- | rev | sort | rev | while read -r file; do
				encapsulate_block "$file" "$identifier" "$datasize" "$parsize" | to_graphics > "${absolute}.ocr.${counter}.png"
				echo "<figure><figcaption><b>$(rev <<< "$file")</b></figcaption><img width=\"100%\" src=\"${filename}.ocr.${counter}.png\"></figure>" >> "${absolute}.ocr.html"
				counter=$((counter + 1))
			done
		)
		echo "</body></html>" >> "${absolute}.ocr.html"
	done
}

function decode {
	firstfile=${1:?need a file to decode}

	absolute=$(readlink -e "$firstfile")
	tmpdir=$(mktemp -d)
	prefix=$(dirname "$absolute")/$(basename -s .pdf "$absolute")
	# shellcheck disable=SC2064
	trap "{ rm -rf '$tmpdir'; }" EXIT

	for file in "$@"; do
		echo "Processing: $file"
		file2packets "$file" | while read -r block; do
			unpack_block "$tmpdir" "$block"
		done || echo "Error while processing $file: no blocks found, skipping" 1>&2
	done

	[ -n "$(ls -A "$tmpdir" 2>/dev/null)" ] || (echo "No files detected." 1>&2 && exit 1)

	for folder in $tmpdir/*; do
		identifier=$(basename "$folder")
		lower=$(tr '[:upper:]' '[:lower:]' <<< "$identifier")
		echo "$lower"

		(
			cd "$folder"
			if ! par2 repair -N data.par2; then
				echo "Unable to decode $lower. Please add closeups for the individual barcodes"
				echo "Missing blocks:"
				missing_blocks "$(stat --format="%s" data)" "$(stat --format="%s" data.par2)" found | rev
				exit 1
			else
				cp data "${prefix}.${lower}"
			fi
		)
	done
}

function help {
	cat <<HELPDOC
Usage: $0 (encode | decode) [options] [--] <file>...

Back up files to a printable format and restore afterwards.
The encode mode will convert files into printable html pages.
The decode mode uses scanned images to reconstruct your original file.

Mandatory arguments to long options are mandatory for short options too.
	--blocksize <bytes>		Sets the blocksize to a non-default value. The default blocksize is 720 bytes.
					    Warning, you must use the same blocksize when decoding.
   -d   --dpi <dpi>			Set the render resolution for vector formats like pdf. The default dpi is 300.
   -h	--help				Display this help message.
   -r	--redundancy <percentage>	Determines how many parity blocks will be generated during encoding in terms
   					    of the size of the file that is being encoded. For example, with a redundancy
					    of 100, you can lose up to 50% of the blocks and still restore the file.
					    The default redundancy is 20
	--shrink <pixels>		Internally shrink image by factor of N. Shrinking is accomplished by skipping N-1
					    pixels at a time. This can be useful when processing very large or blurry
					    scans.
   -t	--timeout <milliseconds>	Sets an upper bound on the amount of processing per page. The default timeout is
   					    5000 milliseconds.
   -v	--verbose			Prints information during processing.
	--version			Prints the version.

When encoding a file this utility will create a set of png files and one html
page per file you specified as inputs. This html file should be printed and
stored somewhere offsite.
Decoding will be easier if you print the file with the highest quality setting
available on your printer and with the option enabled to print background
colors. This makes the text less visible which makes it easier for the decoder
to detect the blocks.

Decoding is done by creating a high resolution scan of the pages. Best results
can be achieved by scanning the pages in color mode with a dpi of 600 or more.
The decoder also accepts text files with already decoded blocks. You can use an
application like Barcode Scanner on Android to generate a file like this in
batch mode.
If the decoder complains about missing blocks, you can create additional scans
or close-ups of the indicated blocks. You can find them by looking at the
caption above the printed block.

Examples:
    $0 encode certificate.key
    $0 decode certificate.key.pdf certificate.key.manual.txt

Issues can be reported at https://gitlab.com/thexa4/ocrbackup/issues
HELPDOC
}

function version {
	echo "orcbackup v${version}, back up your files to paper"
}

mode=help
if [[ "${1:-}" = "encode" ]]; then
	mode=encode
	shift
elif [[ "${1:-}" = "decode" ]]; then
	mode=decode
	shift
elif [[ "${1:-}" = "--version" ]]; then
	version
	exit 0
else
	help
	exit 1
fi

OPTIONS=$(getopt --name ocrbackup --options '+hd:t:r:v' --longoptions 'dpi:,shrink:,timeout:,version,help,blocksize:,redundancy:,verbose' -- "$@")
eval set -- "$OPTIONS"

blocksize=720
redundancy=20
verbose=0
dmtx_milliseconds=5000
dmtx_squareness=5
dmtx_min_edge=50
dmtx_dpi=300
dmtx_opts=

while true; do
	case "$1" in

		-h|--help)
			help
			exit 0;;
		-d|--dpi)
			dmtx_dpi=$2
			shift 2;;
		--shrink)
			# shellcheck disable=SC2037
			dmtx_opts="${dmtx_opts} -S"$2""
			shift 2;;
		-t|--timeout)
			dmtx_milliseconds=$2
			shift 2;;
		--version)
			version
			exit 0;;
		--blocksize)
			blocksize=$2
			shift 2;;
		-r|--redundancy)
			redundancy=$2
			shift 2;;
		-v|--verbose)
			verbose=1
			shift;;
		
		--)
			shift
			break;;

		*)
			echo "Unknown parameter $1 found."
			exit 2;;

	esac
done

DMTX_READ_ARGS="--resolution=${dmtx_dpi} --minimum-edge=${dmtx_min_edge} --milliseconds=${dmtx_milliseconds} -q${dmtx_squareness} ${dmtx_opts}"

if [[ "$#" = 0 ]]; then
	echo "Need to specify at least one file" 1>&2
	help
	exit 1
fi

$mode "$@"
